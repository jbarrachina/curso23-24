<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('saludo/(:alpha)', 'Php::saludo/$1');
$routes->get('indice', 'Php::indice');
$routes->get('tabla','Php::tabla');
$routes->get('tabla/(:num)','Php::tabla/$1');
$routes->get('tabla2','Php::tabla2');
$routes->get('tabla2/(:num)','Php::tabla2/$1');
$routes->get('contar/(:alpha)','Php::contarletras/$1');
$routes->get('contarv/(:alpha)','Php::contarvocales/$1');
$routes->get('contarnum/(:num)','Php::contarelementos/$1');
$routes->get('sumar','Php::sumar');
$routes->post('letranif','Php::letranif');
$routes->get('formulario1','Php::formulario1');
$routes->get('formcalculadora','Php::mostrar_form_calculadora');
$routes->post('calculador','Php::calculador');
//sorteo de proyectos
$routes->get('sorteo','Php::sorteo');

//calendar
$routes->get('calendar/info','Calendar::index');
$routes->get('calendar/(:num)/(:num)','Calendar::muestra_mes/$1/$2');
$routes->get('calendar2/(:num)/(:num)','Calendar::muestra_mes_2/$1/$2');

//cifrador
$routes->get('hash','Cifrador::hash');
$routes->get('cifrador/form_file','Cifrador::form_file');
$routes->post('cifrador/upload','Cifrador::upload');
$routes->get('cifrador','Cifrador::index');
$routes->get('cifrador/prueba','Cifrador::prueba');

$routes->get('sodium','Cifrador::sodium');
$routes->get('sodium1','Cifrador::sodium');

//faker
$routes->get('faker','MyFaker::index');
$routes->get('faker/imagenes','MyFaker::imagenes');
$routes->get('faker/insertar/(:num)','MyFaker::insertarClientes/$1');

//cliente
$routes->get('clientes','ClienteController::index');

//Médico
$routes->get('medico','MedicoController::index');
$routes->get('medicos','MedicoController::index');
$routes->get('medicos/crear/(:num)','MedicoController::crear/$1');
$routes->get('medicos/insertar','MedicoController::insertar');
$routes->post('medicos/insertar','MedicoController::insertar');

$routes->get('medicos/editar/(:num)','MedicoController::editar/$1');
$routes->post('medicos/editar/(:num)','MedicoController::editar/$1');
$routes->get('medicos/borrar/(:num)','MedicoController::borrar/$1');

//Ficheros
$routes->get('fichero','FileController::index');
$routes->post('fichero/upload','FileController::upload');