<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
        <h2><?= $titulo ?></h2>

        <table id="mytabla" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id.</th>                 
                    <th>Nombre</th>
                    <th>E-mail</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($clientes as $cliente): ?>
                <tr>
                    <td><?= $cliente->id ?></td>
                    <td><?= $cliente->apellido1 ?> <?= $cliente->apellido2 ?>, <?= $cliente->nombre ?></td>
                    <td><?= $cliente->email ?> </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.js"></script>
        <script>
            $(document).ready( function () {
                $('#mytabla').DataTable();
            } );
        </script>    
    </body>
</html>