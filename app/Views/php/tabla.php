<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
    <body>
        <h2> La tabla del <?= $numero ?> </h2>
        
        <table class="table table-bordered table-striped">
        <?php for($i=0;$i<=10;$i++): ?>
            <tr>
                <td class="text-info"><?= $i ?></td>
                <td> x </td>
                <td><?= $numero ?></td>
                <td> = </td>
                <td><?= $numero*$i ?></td>
            </tr>
        <?php endfor; ?>
        </table>     
    </body>
</html>

