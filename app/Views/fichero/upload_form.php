<?= $this->extend('templates/default') ?>

<?= $this->section('title')?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>

    <?= form_open_multipart('fichero/upload') ?>
        <div class="custom-file">
            <?= form_upload('fichero','',['class'=>'custom-file-input','id'=>'customFile','onchange'=>'readURL(this);']) ?>
            <?= form_label('Buscar Fichero','customFile',['class'=>'custom-file-label'])?>    
        </div>    
        <?= form_submit('submit', 'Subir') ?>
        
    <?= form_close()?>
    <div class="col-12">
        <img id="visor" class="bg-warning"  width="90px">
    </div>

    
<?= $this->endSection()?>

