<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
        <h2><?= $titulo ?></h2>
        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <?= form_open('medicos/insertar') ?>   
            <div class="form-group">
                <?= form_label('Nombre:','nombre') ?>
                <?= form_input('nombre',set_value('nombre',''),['id'=>'nombre','class'=>'form-control'])?>
            </div>
            <div class="form-group">
                <?= form_label('1er Apellido:','apellido1') ?>
                <?= form_input('apellido1',set_value('apellido1',''),['id'=>'apellido1','class'=>'form-control'])?>
            </div>
            <div class="form-group">
                <?= form_label('2º Apellido:','apellido2') ?>
                <?= form_input('apellido2',set_value('apellido2',''),['id'=>'apellido2','class'=>'form-control'])?>
            </div>
            <div class="form-group">
                <?= form_label('Email:','email') ?>
                <?= form_input('email',set_value('email',''),['id'=>'email','class'=>'form-control'])?>
            </div>
            <div class="form-group">
                 <?= form_label('Especialidad:','especialidad') ?>
                <?= form_dropdown('especialidad',$especialidades,set_value('especialidad','1'),['class'=>"form-control",'id'=>'especialidad'])?>
            </div>
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </body>
</html>    
