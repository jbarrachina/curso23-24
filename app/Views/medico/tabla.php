<?= $this->extend('templates/default') ?>

<?= $this->section('title')?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>
    <table id="mytabla" class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Foto</td>
                <td>pruebas</td>
                <th>Id.</th>                 
                <th>Nombre</th>
                <th>E-mail</th>
                <th>Especialidad</th>
                <th>Acciones</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach($medicos as $medico): ?>
            <tr>
                <td>
                   <img src="assets/img/medicos/fotos/09<?= str_pad($medico->id,4,'0',STR_PAD_LEFT) ?>.jpg" width="30px">
                    
                </td>
                <td>
                    <img src="assets/img/medicos/fotos/09<?= str_pad($medico->id,4,'0',STR_PAD_LEFT) ?>.jpg" width="30px">
                </td>
                <td><?= $medico->id ?></td>
                <td><?= $medico->apellido1 ?> <?= $medico->apellido2 ?>, <?= $medico->nombre ?></td>
                <td><?= $medico->email ?> </td>
                <td><?= $medico->especialidad ?> </td>
                <td><a href="<?= site_url('medicos/editar/'.$medico->id)  ?>">
                        <span class="bi bi-pencil"></span>
                    </a>
                    <a href="<?= site_url('medicos/borrar/'.$medico->id)  ?>" onclick="return confirm('Estás seguro de que quieres borrar el médico seleccionado')">
                        <span class="bi bi-trash text-danger"></span>
                    </a>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?= $this->endSection()?>
        