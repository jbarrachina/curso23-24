<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of FileController
 *
 * @author jbarrachinab
 */
class FileController extends BaseController {
    //formulario para subir seleccionar el fichero a subir    
    public function index(){
        helper('form');
        $data['titulo'] = "Subir un fichero";
        return view('fichero/upload_form',$data);   
    }
    
//subir y procesar el fichero 
    public function upload(){
        $file = $this->request->getFile('fichero'); //seleccion el fichero  
        $name = $file->getClientName(); //vemos el nombre original. incluye extension
        $file->store('prueba',$name); //sube a la ubicación por defecto: directorio uploads
        if ($file->hasMoved()) { 
            echo 'The file has already been moved.';
        }
    }
}
