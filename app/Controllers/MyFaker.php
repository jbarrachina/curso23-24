<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\ClienteModel;


/**
 * Description of Faker
 *
 * @author jose
 */
class MyFaker extends BaseController{
    public function index(){
// use the factory to create a Faker\Generator instance
        $faker = \Faker\Factory::create('es_ES');
//personaliza el generador        
        //$faker->addProvider(new \Faker\Provider\es_ES\Person($faker));
// generate data by calling methods
        for ($i=1;$i<100;$i++){
            echo $faker->dni(),' => ',$faker->lastname(),' ',$faker->lastname(),', ',$faker->firstName(),'<br>';
        }    
// 'Vince Sporer'
        echo $faker->email().'<br>';
// 'walter.sophia@hotmail.com'
        echo '<p>'.$faker->text().'</p>';
// 'Numquam ut mollitia at consequuntur inventore dolorem.'
        
// Generates a Documento Nacional de Identidad (DNI) number
        echo $faker->dni().'<br>'; // '77446565E'
// Genera una cuenta bancaria        
        echo $faker->bankAccountNumber().'<br>'; // "ES5285748762396535068585"

// Generates a Código de identificación Fiscal (CIF) number
        echo $faker->vat().'<br>'; // "A35864370"
// Generates a mobile phone number
        echo $faker->mobileNumber().'<br>'; // +34 612 12 24
        
        echo $faker->address().'<br>';
    }
    
    public function imagenes(){
         $faker = \Faker\Factory::create();
         $faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));
         echo '<table>';
         for($i=1;$i<=5;$i++){
             echo '<tr>';
             for ($j=1;$j<=4;$j++){
                 echo '<td>';
                    echo '<img src="',$faker->imageUrl(500,200,true),'">';
                 echo '</td>';   
             }
             echo "</tr>\n";
         }
         echo '</table>';
    }
    
    public function insertarClientes(int $numero=5){
        $clienteModel = new ClienteModel(); //crear el objeto
        $faker = \Faker\Factory::create('es_ES');
        for ($i = 1; $i <= $numero; $i++) {
            $data = [
                'nombre' => $faker->firstname(),
                'apellido1' => $faker->lastname(),
                'apellido2' => $faker->lastname(),
                'email' => $faker->email(),
            ]; 

// Inserts data and returns inserted row's primary key
            $clienteModel->insert($data); 
        }
        
        $clientes = $clienteModel->findAll();
        echo '<pre>';
        print_r($clientes);
        echo '</pre>';
        
    }
    
}
