<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\MedicoModel;
use App\Models\EspecialidadModel;

/**
 * Description of MedicoController
 *
 * @author jbarrachinab
 */
class MedicoController extends BaseController {

    //controlador que mostrará a todos los clientes    
    public function index() {
        $medicos = new MedicoModel();
        $data['titulo'] = 'Directorio de Médicos';
        $data['medicos'] = $medicos
                ->select('medicos.id,upper(medicos.apellido1) as apellido1,medicos.apellido2')
                ->select('medicos.nombre,medicos.email, especialidades.especialidad')
                ->join('especialidades', 'especialidades.id=medicos.especialidad', 'LEFT')
                ->findAll();
        return view('medico/tabla', $data);
    }

    public function crear(int $numero) {
        $medico = new MedicoModel(); //crear el objeto
        $faker = \Faker\Factory::create('es_ES');
        for ($i = 1; $i <= $numero; $i++) {
            $data = [
                'nombre' => $faker->firstname(),
                'apellido1' => $faker->lastname(),
                'apellido2' => $faker->lastname(),
                'email' => $faker->email(),
                'especialidad' => rand(1, 10),
            ];

// Inserts data and returns inserted row's primary key
            $medico->insert($data);
        }
    }
    
//unir los dos métodos en uno sólo diferenciandose por get o post
    public function insertar(){
        Helper('form');
        $data['titulo'] = 'Alta Médico';

        $especialidad = new EspecialidadModel();
        $especialidades = $especialidad->findAll(); 
        //cambiar estructura array
        foreach($especialidades as $especialidad){
            $listaEspecialidades[$especialidad->id]=$especialidad->especialidad;
        }
        $data['especialidades'] = $listaEspecialidades;
        if (strtoupper($this->request->getMethod())=='GET'){ //mostramos el formulario
            
            return view('medico/form',$data); 
        } else {
            $medico = $this->request->getPost();
            unset($medico['enviar']);
            $medicoModel = new MedicoModel(); //crear el objeto
            if ($medicoModel->insert($medico)=== false){ //He encontrado un error
                //quiero mostrar los errores
                $data['errores'] = $medicoModel->errors();
                return view('medico/form',$data);
            } else {
                return redirect('medicos');
            }
        }
        
    }
    
    public function editar($id){
        Helper('form');
        $data['titulo'] = 'Modificación Médico';
        
        $especialidad = new EspecialidadModel();
        $especialidades = $especialidad->findAll(); 
        //cambiar estructura array
        foreach($especialidades as $especialidad){
            $listaEspecialidades[$especialidad->id]=$especialidad->especialidad;
        }
        $data['especialidades'] = $listaEspecialidades;
        
        $medicoModel = new MedicoModel(); //para acceder a la B.D.
        $data['medico'] = $medicoModel->find($id);
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('medico/form_edit',$data);
        } else {
            $medico = $this->request->getPost();
            unset($medico['enviar']);
            $medico['id'] = $id; //para que funcione la regla de validación is_unique
            echo '<pre>';
            print_r($medico);
            echo '</pre>';
            $medicoModel = new MedicoModel(); //crear el objeto
            if ($medicoModel->update($id,$medico)===false){
               //quiero mostrar los errores
                $data['errores'] = $medicoModel->errors();
                return view('medico/form_edit',$data); 
            } else {
                return redirect('medicos');
            }
        }
        
    }
    
    public function guardar_editar($id){
        $medico = $this->request->getPost();
        unset($medico['enviar']);
        echo '<pre>';
        print_r($medico);
        echo '</pre>';
        $medicoModel = new MedicoModel(); //crear el objeto
        $medicoModel
                ->update($id,$medico);
        return redirect('medicos');
    }
    
    public function borrar ($id){
        $medicoModel = new MedicoModel(); //crear el objeto
        $medicoModel
                ->delete($id);
        return redirect()->to('medicos');
    }
    
   

}
