<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Php
 *
 * @author jbarrachinab
 */
class Php extends BaseController{
    
/*************************************************
 * Hay que dar una explicación, comentar los programas
 ****************************************************/    
    public function saludo($amigo){
        for ($i=1;$i<=10;$i++){ //repite 100 veces, del 1 al 100
            echo "<b>hola</b> amigo -> $i $amigo <br>  \n";
            
        }  
        echo "El nombre de mi amigo/a tiene ".strlen($amigo)." caracteres";
        for ($i=0; $i<strlen($amigo);$i++){
            echo $amigo[$i]."<br>";
        }
    }
/*********************************************************
 * Función que muestra la página de "inicio" del todos los equipos 
 *********************************************************/
    public function indice(){
        for($i=0;$i<9;$i++){
            echo "<p>";
            echo "<a href='http://ed$i.ausias.net' title='Equipo de Desarrollo $i'>";
            echo "Equipo de Desarrollo ".$i;
            echo "</p>\n";
        }
    }
    
/*********************************************************
 * Muestra la tabla del multiplicar de un número entero
 *********************************************************/

    public function tabla(int $num = 7){
        for ($i=0; $i<=10 ; $i++){
            echo "<p>";
            echo "$num x $i = ",$num*$i;
            echo "</p>"; 
        }
    }
    
/*********************************************************
 * Sortea los proyectos entre los equipos
 */    
    public function sorteo(){
       $equipos = ['ed1','ed3','ed5','ed6','ed7','ed8','ed9'];
       shuffle($equipos);
       echo "La nueva ordenación de los equipos";
       echo '<pre>';
       print_r($equipos);
       echo "La asignación sera: <br>";
       for ($i=1;$i<=count($equipos);$i++){
           echo "El proyecto $i será desarrollado por ";
           echo "el Equipo ".$equipos[$i-1];
           echo "<br>";
       }
    }
    
 /*********************************************************
 * Muestra la tabla del multiplicar de un número entero y
 * utiliza una vista
 *********************************************************/

    public function tabla2(int $num = 7){
        $data['numero'] = $num; //parámetro para la vista
        return view('php/tabla', $data); //los parámetros se pasan como segundo argumento
    }   
    
 /********************************************************
  * Función que cuenta las letras a de una palabra
  **********************************************************/
    public function contarletras ($palabra){
        $contador = 0;
        for ($i=0;$i<strlen($palabra);$i++){
            if (strtoupper($palabra[$i])=='A'){
                $contador++;
            }
        }
        echo "la palabra $palabra tiene $contador aes";
        
    }

/********************************************************
  * Función que cuenta las vocales a minúscula de una palabra
  **********************************************************/
    public function contarvocales ($palabra){
        $contador = 0;
        for ($i=0;$i<strlen($palabra);$i++){
            if (strpos("AaEeIiOoUu",$palabra[$i])!==FALSE){
                $contador++;
            }
        }
        echo "la palabra $palabra tiene $contador vocales";
    }    
    
    
/***********************************************************
 * Función que sume los número de un array
 ***********************************************************/
    public function sumar(){
        $vector = [6,7,8,9,2,12,21,2,6,6];
        $sum = 0;
        foreach($vector as $elemento){ //$elemento toma el valor de todos los elementos del array
            $sum+=$elemento; //$sum = $sum + $elemento
        }
        echo "La suma de todos los elementos del vector es $sum";
    }
/************************************************************
 * Contar el número de veces que aparece un elemento que se pasa
 * como parámetro en un array que se define en la función
 *************************************************************/ 
    public function contarelementos(int $num){
        $vector = [2,3,6,3,6,2,6,3,[4,6,7],6,2,9];
        $contador = 0;
        foreach($vector as $indice => $elemento){
            if ($elemento == $num){
                $contador++;
            }
        }
        echo "el número de ocurrencias de $num es $contador";
        echo '<pre>';
        print_r($vector); //mostrar estructuras complejas.
    }
    
    public function letranif(){
        $dni = $this->request->getPost('numero');
        echo "La letra del $dni es ".$this->calcularletra($dni);
    }
/***********************************************
 * Nuestro primer y más básico formulario
 **********************************************/    
    
    public function formulario1(){
        echo '<form action="letranif" method="post">';
            echo "<input type='text' name='numero'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
/****************************************************
 * Llamar a una vista que muestre un calculadora
 ****************************************************/    
    public function mostrar_form_calculadora(){
        return view('php/form_calculadora');
    }
    
/****************************************************
 * Mostrar el resultado
 ****************************************************/    
    public function calculador(){
        $valores = $this->request->getPost();
        echo '<pre>';
        print_r($valores);
    }
    
/******************************************
 * Función que devuelve una letra
 * que corresponde a la letra del NIF
 */
    private function calcularletra($documento){
        $letras = "TRWAGMYFPDXBNJZSQVHLCKEO";
        $resto = $documento % 23;
        return $letras[$resto];
    }
    
    
    
      
}













