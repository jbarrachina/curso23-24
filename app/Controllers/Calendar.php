<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Calendar
 *
 * @author jbarrachinab
 */
class Calendar extends BaseController{
    
    public function index(){
        phpinfo();
    }
    
    public function muestra_mes(int $mes=10, $anyo=2023){
        $fecha = strtotime("$anyo-$mes-01");
        echo date('w',$fecha),'<br>';
        echo date('t',$fecha),'<br>'; 
        echo date('d-m-y',$fecha);
        
        echo '<br>';
        $empezar = date('w',$fecha);
        if ($empezar > 0) {
            $empezar;
        } else {
            $empezar = 7;
        }
        $acabar = date('t',$fecha)+$empezar;
        $i=1; //contador de cuadros
        $dia=1; //contador de días del 
        while($i<=42){
           if ($i < $empezar || $i >= $acabar){
              echo '[  ]';
           } else {
              echo "[ $dia ]";
              $dia++;
           }
           if ($i%7==0) {
               echo '<br>';
           }
           $i++;
        }
        
    }
    
    public function muestra_mes_2(int $mes = 10, $anyo = 2023) {
        echo '<h1>Calendario</h1>';
        $fecha = strtotime("$anyo-$mes-01");
        echo date('d-m-y', $fecha);
        $empezar = date('w', $fecha);
        if ($empezar == 0) {
            $empezar--;
        } else {
            $empezar = 6;
        }
        //primer bucle
        for ($i = 1; $i <= $empezar; $i++) {
            echo "[ ]";
        }
        //segundo bucle
        $dias_mes = date('t', $fecha);
        for ($i = 1; $i <= $dias_mes; $i++) {
            echo "[ $i ]";
            if (($i + $empezar) % 7 == 0) {
                echo '<br>';
            }
        }
        //tercer bucle
        $resto = ($dias_mes + $empezar) % 7;
        for ($i = $resto; $resto <= 7; $i++) {
            echo "[ ]";
        }
    }

}
