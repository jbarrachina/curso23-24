<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Cifrador
 *
 * @author jose
 */
class Cifrador extends BaseController {
    
/******************************************************************************
 * Cifrar y descifrar con openssl
 *****************************************************************************/    

    public function index(){
        $privateKey = openssl_pkey_new(array(
            'private_key_bits' => 2048, // Tamaño de la llave
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ));
        // Guardar la llave privada en el archivo private.key. No compartir este archivo con nadie
        openssl_pkey_export_to_file($privateKey, WRITEPATH.'/private.key');

        // Generar la llave pública para la llave privada
        $a_key = openssl_pkey_get_details($privateKey);

        // Guardar la llave pública en un archivo public.key.
        // Envía este archivo a cualquiera que quiera enviarte datos encriptados
        file_put_contents(WRITEPATH.'/public.key', $a_key['key']);

        // Libera la llave privada
        openssl_free_key($privateKey);

        // Datos a enviar
        $texto = 'Yo he visto cosas que vosotros no creeríais. Atacar naves en '
                . 'llamas más allá de Orión. He visto rayos-C brillar en la '
                . 'oscuridad cerca de la Puerta de Tannhäuser. Todos esos '
                . 'momentos se perderán en el tiempo, como lágrimas en la '
                . 'lluvia.';
        echo $texto;
        // Comprimir los datos a enviar
        $texto = gzcompress($texto);
        // Obtener la llave pública
         
        echo '<p>file://'.WRITEPATH.'public.key</p>';
        $publicKey = openssl_pkey_get_public('file://'.WRITEPATH.'public.key');
        if (! $publicKey){
            echo "Error recuperando la clave";
        }
        $a_key = openssl_pkey_get_details($publicKey);
        
        // Encriptar los datos en porciones pequeñas, combinarlos y enviarlo
        $chunkSize = ceil($a_key['bits'] / 8) - 11;
        $output = '';
        while ($texto) {
            $chunk = substr($texto, 0, $chunkSize);
            $texto = substr($texto, $chunkSize);
            $encrypted = '';
            if (!openssl_public_encrypt($chunk, $encrypted, $publicKey)) {
                die('Ha habido un error al encriptar');
            }
            $output .= $encrypted;
        }
        openssl_free_key($publicKey);
        // Estos son los datos encriptados finales a enviar:
        $encrypted = $output;
        echo "<h2>Datos Cifrados</h2>";
        echo $encrypted;
        echo '<p>',base64_encode($encrypted),'</p>';
        
    // Obtener la llave privada
    if (!$privateKey = openssl_pkey_get_private('file://'.WRITEPATH.'private.key'))
    {
        die('No se ha podido obtener la llave privada');
    }
    $a_key = openssl_pkey_get_details($privateKey);
    // Desencriptar los datos de las porciones
    $chunkSize = ceil($a_key['bits'] / 8);
    $output = '';
    while ($encrypted)
    {
        $chunk = substr($encrypted, 0, $chunkSize);
        $encrypted = substr($encrypted, $chunkSize);
        $decrypted = '';
        if (!openssl_private_decrypt($chunk, $decrypted, $privateKey))
        {
            die('Fallo al desencriptar datos');
        }
        $output .= $decrypted;
    }
    openssl_free_key($privateKey);
    // Descomprimir los datos
    $output = gzuncompress($output);
    //Mostrar
    echo "<h2>Datos Descifrados</h2>";
    echo  $output;
        
    }

/*******************************************************************************
 *  Cifrar y descifrar con sodium
 *  https://github.com/jedisct1/libsodium-php
 *  https://php.watch/articles/modern-php-encryption-decryption-sodium
 ******************************************************************************/    
 
    //cifrado simétrico
    
    public function sodium(){
        
        //Generamos una clave  sodium_crypto_aead_aes256gcm_keygen(): string
        $secretKey = sodium_crypto_secretbox_keygen();
        echo "<h2>Clave</h2>";
        echo bin2hex($secretKey);
        $message = 'Yo he visto cosas que vosotros no creeríais. Atacar naves en '
                . 'llamas más allá de Orión. He visto rayos-C brillar en la '
                . 'oscuridad cerca de la Puerta de Tannhäuser. Todos esos '
                . 'momentos se perderán en el tiempo, como lágrimas en la '
                . 'lluvia.';

        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES); //cada algoritmo tiene un tamaño
        echo "<h2>Nonce</h2>";
        echo bin2hex($nonce);
        
        //ciframos el mensaje - sodium_crypto_aead_aes256gcm_encrypt
        $encryptedMessage = sodium_crypto_secretbox($message, $nonce, $secretKey);
        
        echo "<h2>Mensaje Cifrado</h2>";
        echo bin2hex($encryptedMessage);
        
        //desciframos sodium_crypto_aead_aes256gcm_decrypt
        $decryptedMessage = sodium_crypto_secretbox_open($encryptedMessage, $nonce, $secretKey);
        echo "<h2>Mensaje Descifrado</h2>";
        echo $decryptedMessage;
    }
    
    //utilizando una clave derivada de una contraseña
    
    public function sodium1(){
        $message = 'Yo he visto cosas que vosotros no creeríais. Atacar naves en '
               . 'llamas más allá de Orión. He visto rayos-C brillar en la '
               . 'oscuridad cerca de la Puerta de Tannhäuser. Todos esos '
               . 'momentos se perderán en el tiempo, como lágrimas en la '
               . 'lluvia.';
        $password = 'password';
        $alg = SODIUM_CRYPTO_PWHASH_ALG_DEFAULT;
        $opsLimit = SODIUM_CRYPTO_PWHASH_OPSLIMIT_MODERATE;
        $memLimit = SODIUM_CRYPTO_PWHASH_MEMLIMIT_MODERATE;
        $salt = random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);

        $secretKey = sodium_crypto_pwhash(
                SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_KEYBYTES,
                $password,
                $salt,
                $opsLimit,
                $memLimit,
                $alg
        );
        echo "<h2>Clave</h2>";
        echo bin2hex($secretKey);
        
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES); //cada algoritmo tiene un tamaño
        echo "<h2>Nonce</h2>";
        echo bin2hex($nonce);
        
        //ciframos el mensaje - sodium_crypto_aead_aes256gcm_encrypt
        $encryptedMessage = sodium_crypto_secretbox($message, $nonce, $secretKey);
        
        echo "<h2>Mensaje Cifrado</h2>";
        echo bin2hex($encryptedMessage);
        
        //desciframos sodium_crypto_aead_aes256gcm_decrypt
        $decryptedMessage = sodium_crypto_secretbox_open($encryptedMessage, $nonce, $secretKey);
        echo "<h2>Mensaje Descifrado</h2>";
        echo $decryptedMessage;
    }
    
    public function hash(){
        $message = 'Yo he visto cosas que vosotros no creeríais. Atacar naves en '
               . 'llamas más allá de Orión. He visto rayos-C brillar en la '
               . 'oscuridad cerca de la Puerta de Tannhäuser. Todos esos '
               . 'momentos se perderán en el tiempo, como lágrimas en la '
               . 'lluvia';
        //visualizamos todas las funciones hash disponibles
        echo '<pre>';
        print_r(hash_algos()); //hash_algos — Devuelve una lista con los algoritmos de cifrado soportados
        echo '</pre>';
        $resumen = hash('sha512',$message); //Generar un valor hash (resumen de mensaje)
        echo "<h2>El valor resumen</h2>";
        echo $resumen;
    }
    
//formulario para subir seleccionar el fichero a subir    
    public function form_file(){
        return view('cifrador/upload_form');   
    }
    
//subir y procesar el fichero 
    public function upload(){
        $file = $this->request->getFile('fichero'); //seleccion el fichero  
        $name = $file->getClientName(); //vemos el nombre original. incluye extension
        $file->store('cifrado',$name); //sube a la ubicación por defecto: directorio uploads
        if (!$file->hasMoved()) { //si no ha tenido exito la subido.
            //$filepath = WRITEPATH . 'uploads/' . $file->store();
            //echo $filepath;
            //$data = ['uploaded_fileinfo' => new File($filepath)];
        } else {
            echo 'The file has already been moved.';
        }
        $resumen = hash_file('sha512',WRITEPATH . 'uploads/cifrado/'.$name);
        echo "<h2>El valor resumen</h2>";
        echo $resumen;
    }
    
//utilizar la librería que viene con CodeIgniter
    public function prueba(){
        $encrypter = \Config\Services::encrypter(); //decimos que utilizamos el cifrador que viene con CI4
        
        $texto = 'Yo he visto cosas que vosotros no creeríais. Atacar naves en '
                . 'llamas más allá de Orión. He visto rayos-C brillar en la '
                . 'oscuridad cerca de la Puerta de Tannhäuser. Todos esos '
                . 'momentos se perderán en el tiempo, como lágrimas en la '
                . 'lluvia.';
        echo $texto;
        $ciphertext = $encrypter->encrypt($texto);
        echo "<h2>Mensaje Cifrado</h2>";
        echo bin2hex($ciphertext);
        echo "<h2>Mensaje Descifrado</h2>";
        echo $encrypter->decrypt($ciphertext);
        
    }
}
