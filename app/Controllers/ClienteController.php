<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\ClienteModel;

/**
 * Description of Cliente
 *
 * @author jbarrachinab
 */
class ClienteController extends BaseController {
    
//controlador que mostrará a todos los clientes    
    public function index(){
        $clientes = new ClienteModel();
        $data['titulo'] = 'Clientes';
        $data['clientes'] = $clientes->findAll();
        return view('cliente/tabla',$data);
    }
}
