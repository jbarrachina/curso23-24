<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of MedicoModel
 *
 * @author jbarrachinab
 */
class MedicoModel extends Model {
    protected $table      = 'medicos';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['nombre', 'apellido1', 'apellido2',  'email', 'especialidad'];
    
    protected $validationRules = [
        'id'    => 'numeric',
        'nombre'     => 'required|max_length[30]|min_length[3]',
        'apellido1'     => 'required|max_length[30]|min_length[3]',
        'apellido2'     => 'trim|strtoupper',
        'email'        => 'required|max_length[254]|valid_email|is_unique[medicos.email,id,{id}]',
        'especialidad'     => 'required|is_not_unique[especialidades.id]',
    ];
    protected $validationMessages = [
        'email' => [
            'is_unique' => 'Este correo ya está siendo utilizado.',
            'required' => 'No puedes dejar el email en blanco.'
        ],
    ];
}
