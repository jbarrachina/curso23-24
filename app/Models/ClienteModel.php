<?php

namespace App\Models;

use CodeIgniter\Model; 

/**
 * Description of Clientes
 *
 * @author jbarrachinab
 */
class ClienteModel extends Model{
    protected $table      = 'clientes';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['nombre', 'apellido1', 'apellido2',  'email'];
}
