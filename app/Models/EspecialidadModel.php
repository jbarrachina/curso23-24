<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of EspecialidadModel
 *
 * @author jbarrachinab
 */
class EspecialidadModel extends Model {
    protected $table      = 'especialidades';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';
}
